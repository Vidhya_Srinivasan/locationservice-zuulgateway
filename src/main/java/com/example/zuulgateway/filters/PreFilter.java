package com.example.zuulgateway.filters;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class PreFilter extends ZuulFilter {

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 10;
  }

  @Override
  public boolean shouldFilter() {
	 return true;
  }

  @Override
  public Object run() {
	  
	RequestContext ctx = RequestContext.getCurrentContext();
    HttpServletRequest request = ctx.getRequest();
    /*try {
		ctx.setRouteHost(new URL("http://172.16.34.34:8050"));
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
    
    
 
    System.out.println("PreFilter: " + 
    								String.format("%s request to %s", 
    												request.getMethod(),
    												request.getRequestURL().toString()));
    return null;
  }

}