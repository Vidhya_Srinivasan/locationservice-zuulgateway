package com.example.zuulgateway.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.zuulgateway.model.LocationDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class CallRestAPI implements CommandLineRunner{
    
	private static void callRestService() {

		RestTemplate restTemplate = new RestTemplate();
        
        ResponseEntity<LocationDTO> locationResponse = restTemplate.
        											exchange("http://172.16.34.34:8050/LocationService/api/"
        						                               + "location/GetLocationDetails?zipCode=90010",
        													 HttpMethod.GET,
        													 null,
        													 LocationDTO.class);
        
        ObjectMapper mapper = new ObjectMapper();
    	try {
			System.out.println(mapper.writerWithDefaultPrettyPrinter()
									  .writeValueAsString(locationResponse.getBody()));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void run(String... arg0) throws Exception {
		 callRestService();
		
	}

}
