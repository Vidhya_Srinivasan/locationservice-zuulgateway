package com.example.zuulgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationDTO {
	
private StateDTO[] state;

private CountyDTO[] county;

	public StateDTO[] getState() {
		return state;
	}
	
	public void setState(StateDTO[] state) {
		this.state = state;
	}
	
	public CountyDTO[] getCounty() {
		return county;
	}
	
	public void setCounty(CountyDTO[] county) {
		this.county = county;
	}

}
